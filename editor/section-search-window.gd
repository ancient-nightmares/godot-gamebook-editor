extends WindowDialog

var app = null

onready var SEARCH_TERM_FIELDS = [
	$VBoxContainer/LabelHBoxContainer/LineEdit,
	$VBoxContainer/TitleHBoxContainer/LineEdit,
	$VBoxContainer/ContentHBoxContainer/LineEdit,
	$VBoxContainer/TagsHBoxContainer/LineEdit,
	$VBoxContainer/NotesHBoxContainer/LineEdit
]

func init(app_instance):
	app = app_instance
	clear_search_terms()

func clear_search_terms():
	for field in SEARCH_TERM_FIELDS:
		field.text = ""
	

func check_search_terms():
	for field in SEARCH_TERM_FIELDS:
		if field.text.strip_edges() != "":
			return true
	return false


func _on_Button_pressed():
	app.section_graph.clear_overlays()
	if not check_search_terms():
		GUIUtils.show_information(tr("No search terms were specified"), self)
	else:
		var found = app.search_sections_data(
			$VBoxContainer/LabelHBoxContainer/LineEdit.text.strip_edges().to_lower(),
			$VBoxContainer/TitleHBoxContainer/LineEdit.text.strip_edges().to_lower(),
			$VBoxContainer/ContentHBoxContainer/LineEdit.text.strip_edges().to_lower(),
			$VBoxContainer/TagsHBoxContainer/LineEdit.text.strip_edges().to_lower(),
			$VBoxContainer/NotesHBoxContainer/LineEdit.text.strip_edges().to_lower(),
			$VBoxContainer/LabelHBoxContainer/CheckBox.pressed
		)
		if found.size() == 0:
			GUIUtils.show_information(tr("There are no sections matching given criteria"), self)
		else:
			for node_label in found:
				var node = app.section_graph.get_section(node_label)
				node.overlay = GraphNode.OVERLAY_BREAKPOINT
			self.hide()
			GUIUtils.show_information(tr("Found " + str(found.size()) + " results"), self)
	
