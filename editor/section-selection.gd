extends WindowDialog

var app = null
var _callback = null

onready var modules_list = $VBoxContainer/HSplitContainer/ModuleList
onready var section_list = $VBoxContainer/HSplitContainer/SectionList

func init(app_instance):
	app = app_instance

func update_data(callback_funcrec):
	_callback = callback_funcrec
	modules_list.clear()
	section_list.clear()
	var modules = app.MODULES_DATA.keys()
	modules.sort()
	for module in modules:
		modules_list.add_item(module)
	# Select first module
	modules_list.select(0)
	_on_ModuleList_item_selected(0)


func _on_CancelButton_pressed():
	hide()

func _on_SelectButton_pressed():
	var sections = section_list.get_selected_items()
	if sections:
		var section = section_list.get_item_text(sections[0])
		if _callback:
			_callback.call_func(section)
		hide()
	else:
		GUIUtils.show_information(tr('You have not selected a section'), app)

func _on_ModuleList_item_selected(index):
	# Get module name
	section_list.clear()
	var module = modules_list.get_item_text(index)
	var nodes = app.MODULES_DATA[module]['nodes'].keys()
	nodes.sort()
	for node in nodes:
		section_list.add_item(node)
