extends WindowDialog

var app = null
var _callback = null

onready var expression_input = $VBoxContainer/HBoxExpression/LineEdit

func init(app_instance):
	app = app_instance

func update_data(callback_funcrec, current_expression):
	if not current_expression:
		current_expression = ''
	_callback = callback_funcrec
	expression_input.text = current_expression
	expression_input.grab_focus()

func _on_CancelButton_pressed():
	hide()

func _on_SaveButton_pressed():
	var expression = expression_input.text
	if not expression:
		GUIUtils.show_information(tr('Expression is empty'), app)
		return
	# TODO: Check syntax
	# var obj = Utils.get_script_object(expression)
	if _callback:
		_callback.call_func(expression)
		hide()


func wrap_selected_text(prepend, append):
	# Get selection
	# TODO
	# If there is no selection, selection is full text
	var input = $VBoxContainer/HBoxExpression/LineEdit
	input.text = prepend + input.text + append


func _on_NotButton_pressed():
	wrap_selected_text('not(', ')')


func _on_VisitedButton_pressed():
	wrap_selected_text("visited('@'", ")")


func _on_FirstTimeHereButton_pressed():
	$VBoxContainer/HBoxExpression/LineEdit.append_at_cursor('first_time_here()')
